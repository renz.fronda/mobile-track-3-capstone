package com.example.capstone_track3

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.capstone_track3.databinding.ActivityMainBinding
import com.example.capstone_track3.gallery.ModalFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
        binding.btnOpen.setOnClickListener{
            supportFragmentManager.beginTransaction()
                .replace(R.id.fl_modal_container, ModalFragment())
                .commit()
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

}