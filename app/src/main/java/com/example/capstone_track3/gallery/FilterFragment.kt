package com.example.capstone_track3.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.capstone_track3.R
import com.example.capstone_track3.databinding.FragmentFilterBinding
import com.example.capstone_track3.gallery.viewModel.ModalViewModel

class FilterFragment : Fragment() {

    private var _binding: FragmentFilterBinding? = null
    private var totalBrandFilter = 0
    private var selectedBrandFilter = 0
    private var totalPlanFilter = 0
    private var selectedPlanFilter = 0
    private var totalProductFilter = 0
    private var selectedProductTypeFilter = 0
    private var finalString = ""
    private val modalViewModel: ModalViewModel by activityViewModels()
    private val binding
        get() = checkNotNull(_binding) {
            "Cannot access binding because it is null. Is the view visible?"
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentFilterBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            btnSelectAllBrand.setOnClickListener {
                if (totalBrandFilter<6){
                    selectAllBrand()
                }
            }
            btnSelectAllPlan.setOnClickListener {
                if (totalPlanFilter<6){
                    selectAllPlan()
                }
            }
            btnResetFilters.setOnClickListener {
                resetFilters()
            }
            btnSelectAllProduct.setOnClickListener {
                if (totalPlanFilter<3){
                    selectAllProductType()
                }
            }
            chpBrand1.setOnClickListener {
                toggleBrandFilter(APPLE,binding.chpBrand1.isChecked)
            }
            chpBrand2.setOnClickListener {
                toggleBrandFilter(SAMSUNG,binding.chpBrand2.isChecked)
            }
            chpBrand3.setOnClickListener {
                toggleBrandFilter(HUAWEI,binding.chpBrand3.isChecked)
            }
            chpBrand4.setOnClickListener {
                toggleBrandFilter(OPPO,binding.chpBrand4.isChecked)
            }
            chpBrand5.setOnClickListener {
                toggleBrandFilter(XIAOMI,binding.chpBrand5.isChecked)
            }
            chpBrand6.setOnClickListener {
                toggleBrandFilter(VIVO,binding.chpBrand6.isChecked)
            }
            chpPlan1.setOnClickListener {
                togglePlanFilter(PLAN599,binding.chpPlan1.isChecked)
            }
            chpPlan2.setOnClickListener {
                togglePlanFilter(PLAN799,binding.chpPlan2.isChecked)
            }
            chpPlan3.setOnClickListener {
                togglePlanFilter(PLAN999,binding.chpPlan3.isChecked)
            }
            chpPlan4.setOnClickListener {
                togglePlanFilter(PLAN1299,binding.chpPlan4.isChecked)
            }
            chpPlan5.setOnClickListener {
                togglePlanFilter(PLAN2499,binding.chpPlan5.isChecked)
            }
            chpPlan6.setOnClickListener {
                togglePlanFilter(PLAN2999,binding.chpPlan6.isChecked)
            }
            chpProductType1.setOnClickListener {
                toggleProductivity(MOBILE_PHONE,binding.chpProductType1.isChecked)
            }
            chpProductType2.setOnClickListener {
                toggleProductivity(TABLET,binding.chpProductType2.isChecked)
            }
            chpProductType3.setOnClickListener {
                toggleProductivity(SMART_WATCH,binding.chpProductType3.isChecked)
            }

            btnLowToHigh.setOnClickListener {
                modalViewModel.onPriceLowToHighClicked()
                btnHighToLow.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.v2_functional_text
                    )
                )
                btnLowToHigh.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.v2_functional_primary
                    )
                )
            }
            btnHighToLow.setOnClickListener {
                modalViewModel.onPriceHighToLowClicked()
                btnHighToLow.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.v2_functional_primary
                    )
                )
                btnLowToHigh.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.v2_functional_text
                    )
                )
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun init(){
        with(binding){
            chpBrand1.text = APPLE
            chpBrand2.text = SAMSUNG
            chpBrand3.text = HUAWEI
            chpBrand4.text = OPPO
            chpBrand5.text = XIAOMI
            chpBrand6.text = VIVO
            chpPlan1.text = PLAN599
            chpPlan2.text = PLAN799
            chpPlan3.text = PLAN999
            chpPlan4.text = PLAN1299
            chpPlan5.text = PLAN2499
            chpPlan6.text = PLAN2999
            chpProductType1.text = MOBILE_PHONE
            chpProductType2.text = TABLET
            chpProductType3.text = SMART_WATCH

            btnLowToHigh.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.v2_functional_text
                )
            )
            btnHighToLow.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.v2_functional_text
                )
            )
        }
    }

    private fun resetFilters(){
        finalString = ""
        modalViewModel.resetFilters()
        totalBrandFilter = 0
        selectedBrandFilter = 0
        totalPlanFilter = 0
        selectedPlanFilter = 0
        selectedProductTypeFilter = 0
        modalViewModel.selectedValue.value = finalString
        modalViewModel._numberOfFilterSelected.value = 0
        with(binding) {
            chpBrand1.isChecked = false
            chpBrand2.isChecked = false
            chpBrand3.isChecked = false
            chpBrand4.isChecked = false
            chpBrand5.isChecked = false
            chpBrand6.isChecked = false
            chpPlan1.isChecked = false
            chpPlan2.isChecked = false
            chpPlan3.isChecked = false
            chpPlan4.isChecked = false
            chpPlan5.isChecked = false
            chpPlan6.isChecked = false
            chpProductType1.isChecked = false
            chpProductType2.isChecked = false
            chpProductType3.isChecked = false
            btnLowToHigh.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.v2_functional_text
                )
            )
            btnHighToLow.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.v2_functional_text
                )
            )
        }
    }

    private fun selectAllBrand(){
        finalString = "$APPLE $SAMSUNG $HUAWEI $VIVO $OPPO $XIAOMI "
        modalViewModel.selectedValue.value = finalString
        with(binding) {
            chpBrand1.isChecked = true
            chpBrand2.isChecked = true
            chpBrand3.isChecked = true
            chpBrand4.isChecked = true
            chpBrand5.isChecked = true
            chpBrand6.isChecked = true
        }
        totalBrandFilter = 6 - selectedBrandFilter
        modalViewModel.addNumberOfFilterSelected(totalBrandFilter)
    }

    private fun selectAllPlan(){
        finalString = "$APPLE $SAMSUNG $HUAWEI $VIVO $OPPO $XIAOMI "
        modalViewModel.selectedValue.value = finalString
        with(binding) {
            chpBrand1.isChecked = true
            chpBrand2.isChecked = true
            chpBrand3.isChecked = true
            chpBrand4.isChecked = true
            chpBrand5.isChecked = true
            chpBrand6.isChecked = true
        }
        totalBrandFilter = 6 - selectedBrandFilter
        modalViewModel.addNumberOfFilterSelected(totalBrandFilter)
    }

    private fun selectAllProductType(){
        finalString = "$MOBILE_PHONE $SMART_WATCH "
        modalViewModel.selectedValue.value = finalString
        with(binding) {
            chpProductType1.isChecked = true
            chpProductType2.isChecked = true
            chpProductType3.isChecked = true
        }
        totalProductFilter = 3 - selectedProductTypeFilter
        modalViewModel.addNumberOfFilterSelected(totalProductFilter)
    }

    private fun toggleProductivity(type:String,isChecked: Boolean){
        if(isChecked) {
            finalString += "$type "
            selectedProductTypeFilter += 1
            modalViewModel.addNumberOfFilterSelected(1)
        }else {
            finalString = finalString.replace("$type ", "")
            selectedProductTypeFilter -= 1
            modalViewModel.subtractNumberOfFilterSelected(1)
        }
        modalViewModel.selectedValue.value = finalString
    }
    private fun toggleBrandFilter(string: String, isChecked: Boolean){
        if(isChecked) {
            finalString += "$string "
            selectedBrandFilter += 1
            modalViewModel.addNumberOfFilterSelected(1)
        }else {
            finalString = finalString.replace("$string ", "")
            selectedPlanFilter -= 1
            modalViewModel.subtractNumberOfFilterSelected(1)
        }
        modalViewModel.selectedValue.value = finalString
    }
    private fun togglePlanFilter(string: String, isChecked: Boolean){
        if(isChecked) {
            finalString += "$string "
            selectedPlanFilter += 1
            modalViewModel.addNumberOfFilterSelected(1)
        }else {
            finalString = finalString.replace("$string ", "")
            selectedPlanFilter -= 1
            modalViewModel.subtractNumberOfFilterSelected(1)
        }
        modalViewModel.selectedValue.value = finalString
    }
}

private const val APPLE = "apple"
private const val SAMSUNG = "samsung"
private const val VIVO = "vivo"
private const val HUAWEI = "huawei"
private const val OPPO = "oppo"
private const val XIAOMI = "xiaomi"
private const val PLAN599 = "599"
private const val PLAN799 = "799"
private const val PLAN999 = "999"
private const val PLAN1299 = "1299"
private const val PLAN2499 = "2499"
private const val PLAN2999 = "2999"
private const val MOBILE_PHONE = "MobilePhone"
private const val TABLET = "Tablet"
private const val SMART_WATCH = "Smartwatch"
