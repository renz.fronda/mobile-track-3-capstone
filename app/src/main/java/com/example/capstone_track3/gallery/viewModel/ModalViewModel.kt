package com.example.capstone_track3.gallery.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.capstone_track3.gallery.model.Item
import com.example.capstone_track3.gallery.model.ItemRepository

class ModalViewModel: ViewModel(){

    private val itemsLiveData = MutableLiveData<List<Item>>()
    private val itemRepository = ItemRepository()
    private val _selectedItem = MutableLiveData<Item>()
    private val _isModelOpen = MutableLiveData<Boolean>()

    val selectedValue = MutableLiveData<String>()
    val _numberOfFilterSelected = MutableLiveData<Int>(0)
    val priceLowToHighIsClicked = MutableLiveData<SortOrder>()
    val priceSortingLiveData = MutableLiveData<SortOrder>()
    val itemList: MutableLiveData<List<Item>> = MutableLiveData()

    val numberOfFilterSelected: LiveData<Int>
        get() = _numberOfFilterSelected

    val isModelOpen: LiveData<Boolean>
        get() = _isModelOpen

    fun getSeletedItem(value: Item){
        _selectedItem.value = value
    }

    fun getItemsLiveData(): LiveData<List<Item>>{
        itemsLiveData.value = itemRepository.getItemList()
        return itemsLiveData
    }

    fun clearSharedPreferences(context: Context) {
        val sharedPreferences = context.getSharedPreferences("search_history", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun setIsModelOpen(value: Boolean) {
        _isModelOpen.value = value
    }

    fun onPriceLowToHighClicked() {
        priceSortingLiveData.value = SortOrder.LOW_TO_HIGH
    }

    fun onPriceHighToLowClicked() {
        priceSortingLiveData.value = SortOrder.HIGH_TO_LOW
    }

    fun resetFilters(){
        priceSortingLiveData.value = SortOrder.NONE
    }

    fun addNumberOfFilterSelected(value: Int){
        _numberOfFilterSelected.value = _numberOfFilterSelected.value?.plus(value)
    }

    fun subtractNumberOfFilterSelected(value: Int){
        _numberOfFilterSelected.value = _numberOfFilterSelected.value?.minus(value)
    }




}
enum class SortOrder {
    LOW_TO_HIGH,
    HIGH_TO_LOW,
    NONE
}