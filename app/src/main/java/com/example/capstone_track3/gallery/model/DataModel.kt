package com.example.capstone_track3.gallery.model

import java.time.zone.ZoneOffsetTransitionRule.TimeDefinition

data class Item(val name: String? = null,
                val brand: String? = null,
                val os: String? = null,
                val price: Int? = null,
                val description: String? = null,
                val image: Int? = null,
                val plan: String? = null,
                val tags: String? = null,
                val productType: String? = null,
                var isSelected: Boolean = false
)



