package com.example.capstone_track3.gallery.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.capstone_track3.gallery.model.Item
import com.example.capstone_track3.gallery.model.ItemRepository

class ReceiptViewModel: ViewModel() {

    private val _selectedItem = MutableLiveData<Item>()

    val selectedItem: LiveData<Item>
        get() = _selectedItem

    fun setSelectedItem(item: Item) {
        _selectedItem.value = item
    }

}