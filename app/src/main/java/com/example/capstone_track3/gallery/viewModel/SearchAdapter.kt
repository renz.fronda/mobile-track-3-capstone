import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.capstone_track3.R

class SearchAdapter() : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private val searchHistory = mutableListOf<String>()
    private var clickListener: recentOnItemClickListener? = null

    interface recentOnItemClickListener {
        fun recentOnItemClick(query: String)
    }

    fun setOnItemClickListener(listener: recentOnItemClickListener) {
        this.clickListener = listener
    }

    fun updateSearchHistory(newSearchHistory: List<String>) {
        searchHistory.clear()
        searchHistory.addAll(newSearchHistory)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_search, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(searchHistory[position])
    }

    override fun getItemCount(): Int {
        return searchHistory.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val searchQueryTextView: TextView = itemView.findViewById(R.id.searchQueryTextView)

        fun bind(query: String) {
            searchQueryTextView.text = query
            itemView.setOnClickListener {
                // Handle item click here by passing the selected query to the listener
                clickListener?.recentOnItemClick(query)

            }
        }
    }
}
