package com.example.capstone_track3.gallery

import SearchAdapter
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.capstone_track3.gallery.viewModel.ModalViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.capstone_track3.R
import com.example.capstone_track3.Utils.ItemSpacingDecoration
import com.example.capstone_track3.databinding.FragmentModalBinding
import com.example.capstone_track3.gallery.model.Item
import com.example.capstone_track3.gallery.viewModel.GridAdapter
import com.example.capstone_track3.gallery.viewModel.SortOrder
import com.example.capstone_track3.gallery.viewModel.ReceiptViewModel


class ModalFragment : Fragment(){

    private var _binding: FragmentModalBinding? = null
    private val binding get() = _binding!!
    private lateinit var itemViewModel: ModalViewModel
    private lateinit var itemAdapter: GridAdapter
    private lateinit var sharedPref: SharedPreferences
    private lateinit var recentSearchAdapter: SearchAdapter
    private val receiptViewModel: ReceiptViewModel by activityViewModels()
    private val modalViewModel: ModalViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentModalBinding.inflate(inflater, container, false)
        init()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setComponentsListeners()
        getQueryItems()
        loadSearchHistory()
    }

    private fun init() {
        itemAdapter = GridAdapter()
        recentSearchAdapter = SearchAdapter()
        sharedPref = requireContext().getSharedPreferences(SHARED_PREF_SEARCH_HISTORY, Context.MODE_PRIVATE)
        binding.apply {
            rvGrid.adapter = itemAdapter
            rvRecentSearch.adapter = recentSearchAdapter
            rvRecentSearch.layoutManager = LinearLayoutManager(activity)
            rvGrid.addItemDecoration(ItemSpacingDecoration(10))
            rvGrid.layoutManager = GridLayoutManager(requireContext(), 2)
        }
        childFragmentManager.beginTransaction()
            .replace(R.id.fl_filters_container, FilterFragment())
            .commit()
    }

    private fun setComponentsListeners(){
        modalViewModel.selectedValue.observe(viewLifecycleOwner) { value ->
            itemAdapter.filter.filter(value)
        }
        modalViewModel.priceSortingLiveData.observe(viewLifecycleOwner){sortOrder ->
            when (sortOrder) {
                SortOrder.LOW_TO_HIGH -> {
                    itemAdapter.sortItemsLowToHigh()
                }
                SortOrder.HIGH_TO_LOW -> {
                    itemAdapter.sortItemsHighToLow()
                }
                SortOrder.NONE -> {
                    itemAdapter.sortNone()
                }
            }
        }
        modalViewModel.numberOfFilterSelected.observe(viewLifecycleOwner){
            if (it>0){
                binding.cvNumberHolder.visibility = VISIBLE
                binding.tvNumberOfFiltersSelected.text = it.toString()
            }else{
                binding.cvNumberHolder.visibility = INVISIBLE
            }
            Log.d("tvNumberOfFiltersSelected",it.toString())
        }

        itemAdapter.setOnItemClickListener(object : GridAdapter.searchOnItemClickListener {
            override fun searchOnItemClick(item: Item) {
                binding.clRecentContainer.visibility = GONE
                binding.flFiltersContainer.visibility = GONE
                binding.btnSelect.setOnClickListener {
                    receiptViewModel.setSelectedItem(item)
                    val transaction = parentFragmentManager.beginTransaction()
                    transaction.replace(R.id.fragment_container_view, ReceiptFragment())
                    transaction.addToBackStack(ModalFragment().tag)
                    transaction.commit()
                    fragmentManager?.beginTransaction()?.remove(this@ModalFragment)?.commit()
                }
            }
        })
        recentSearchAdapter.setOnItemClickListener(object : SearchAdapter.recentOnItemClickListener {
            override fun recentOnItemClick(query: String) {
                binding.searchView.setQuery(query, true)
                binding.clRecentContainer.visibility = GONE
                }
            }
        )
        with(binding) {
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    binding.clRecentContainer.visibility = GONE
                    itemAdapter.filter.filter(query)
                    saveSearchQuery(query.toString())
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    binding.clRecentContainer.visibility = VISIBLE
                    checkRecentSearchIfEmpty()
                    if (newText?.isEmpty()== true){
                        getQueryItems()
                    }
                    if (newText.isNullOrEmpty()) {
                        binding.clEmptyView.visibility = VISIBLE
                        binding.rvGrid.visibility = INVISIBLE

                    } else {
                        binding.clEmptyView.visibility = GONE
                        binding.rvGrid.visibility = VISIBLE
                    }
                    return true
                }
            })
            searchView.setOnSearchClickListener {
                binding.clRecentContainer.visibility = VISIBLE
                binding.flFiltersContainer.visibility = GONE
                checkRecentSearchIfEmpty()
            }
            searchView.setOnClickListener {
                binding.clRecentContainer.visibility = VISIBLE
                binding.flFiltersContainer.visibility = GONE
                checkRecentSearchIfEmpty()
            }
            searchView.setOnCloseListener {
                binding.searchView.setQuery("", false)
                itemAdapter.filter.filter("")
                true
            }
            ivCloseModal.setOnClickListener{
                modalViewModel.setIsModelOpen(false)
                fragmentManager?.beginTransaction()?.remove(this@ModalFragment)?.commit()
            }
            btnClearRecentSearch.setOnClickListener {
                sharedPref.edit().remove(SHARED_PREF_SEARCH_HISTORY).apply()
                recentSearchAdapter.updateSearchHistory(emptyList())
                binding.clRecentContainer.visibility = GONE
            }
            ivFilter.setOnClickListener {
                showFilter()
            }
        }
    }

    private fun getQueryItems(){
        itemViewModel = ViewModelProvider(this).get(ModalViewModel::class.java)
        itemViewModel.getItemsLiveData().observe(viewLifecycleOwner) { items ->
            val query = binding.searchView.query
            val filteredItems = if (query.isEmpty()) {
                items
            } else {
                items.filter { item ->
                    item.plan?.contains(query, ignoreCase = true) ?: false ||
                    item.name?.contains(query, ignoreCase = true) ?: false ||
                    item.brand?.contains(query, ignoreCase = true) ?: false ||
                    item.productType?.contains(query, ignoreCase = true) ?: false ||
                    item.os?.contains(query, ignoreCase = true) ?: false
                }
            }
            itemAdapter.setItems(filteredItems)
        }
    }

    private fun showFilter() {
        with(binding){
            if(flFiltersContainer.isVisible){
                flFiltersContainer.visibility = GONE
            }
            else{
                flFiltersContainer.visibility = VISIBLE
                clRecentContainer.visibility = GONE
            }
        }
    }

    private fun saveSearchQuery(query: String) {
        val historySet = sharedPref.getStringSet(SHARED_PREF_SEARCH_HISTORY, HashSet()) ?: HashSet()
        historySet.add(query)
        sharedPref.edit().putStringSet(SHARED_PREF_SEARCH_HISTORY, historySet).apply()
        recentSearchAdapter.updateSearchHistory(historySet.toList())
    }


    private fun loadSearchHistory() {
        val historySet = sharedPref.getStringSet(SHARED_PREF_SEARCH_HISTORY, HashSet()) ?: HashSet()
        recentSearchAdapter.updateSearchHistory(historySet.toList())
    }

    private fun checkRecentSearchIfEmpty(){
        val allEntries = sharedPref.all
        with(binding){
            if (allEntries.isEmpty()) {
                tvRecentSearchHeader.visibility = GONE
                btnClearRecentSearch.visibility = GONE
            } else {
                tvRecentSearchHeader.visibility = VISIBLE
                btnClearRecentSearch.visibility = VISIBLE
            }
        }
    }
}

const val SHARED_PREF_SEARCH_HISTORY = "search_history"
