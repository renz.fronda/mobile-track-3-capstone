package com.example.capstone_track3.gallery.viewModel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.capstone_track3.R
import com.example.capstone_track3.gallery.model.Item

class GridAdapter : RecyclerView.Adapter<GridAdapter.ViewHolder>(), Filterable {
    private var itemList = listOf<Item>()
    private var filteredItemList = listOf<Item>()
    private var listener: searchOnItemClickListener? = null
    var selectedItemPos = -1

    interface searchOnItemClickListener {
        fun searchOnItemClick(item: Item)
    }

    fun setItems(items: List<Item>) {
        itemList = items
        filteredItemList = items
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(listener: searchOnItemClickListener) {
            this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = filteredItemList[position]
        holder.bind(item, position)
    }

    override fun getItemCount(): Int {
        return filteredItemList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvName: TextView = itemView.findViewById(R.id.tv_name)
        private val tvDescription: TextView = itemView.findViewById(R.id.tv_description)
        private val tvPrice: TextView = itemView.findViewById(R.id.tv_price)
        private val ivPhone: ImageView = itemView.findViewById(R.id.iv_phone_image)
        private val cvTag: CardView = itemView.findViewById(R.id.cv_tags)
        private val tvTag: TextView = itemView.findViewById(R.id.tv_tag_name)

        fun bind(item: Item, position: Int) {
            itemView.setOnClickListener {
                listener?.searchOnItemClick(item)
                selectedItemPos = position
                notifyDataSetChanged()
            }
            tvName.text = item.name
            tvDescription.text = item.description
            tvPrice.text = PESO_SYMBOL + item.price

            val pink = ContextCompat.getColor(itemView.context, R.color.v2_tm_light) // Replace with your desired color resource
            val blue = ContextCompat.getColor(itemView.context, R.color.v2_prepaid_blue) // Replace with your desired color resource
            val violet = ContextCompat.getColor(itemView.context, R.color.v2_broadband_violet) // Replace with your desired color resource

            when(item.tags){
                TAG_BEST_SELLER -> {
                    tvTag.text  = TAG_BEST_SELLER
                    cvTag.setCardBackgroundColor(pink)
                }
                TAG_NEW -> {
                    tvTag.text  = TAG_NEW
                    cvTag.setCardBackgroundColor(blue)
                }
                TAG_PRE_ORDER -> {
                    tvTag.text  = TAG_PRE_ORDER
                    cvTag.setCardBackgroundColor(violet)
                }
                else -> TAG_EMPTY
            }

            item.image?.let { ivPhone.setImageResource(it) }
            if (selectedItemPos == position) {
                itemView.setBackgroundResource(R.drawable.device_card_selected)
            } else {
                itemView.setBackgroundResource(R.drawable.device_card_unselected)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val query = constraint.toString().trim()
                val searchTerms = query.split("\\s+".toRegex())

                val filteredList = if (query.isEmpty()) {
                    itemList
                } else {
                    itemList.filter { item ->
                        searchTerms.any { term ->
                            item.os?.contains(term, ignoreCase = true) ?: false ||
                            item.plan?.contains(term, ignoreCase = true) ?: false ||
                            item.brand?.contains(term, ignoreCase = true) ?: false ||
                            item.productType?.contains(query, ignoreCase = true) ?: false ||
                            item.name?.contains(term, ignoreCase = true) ?: false
                        }
                    }
                }

                val results = FilterResults()
                results.values = filteredList
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredItemList = results?.values as? List<Item> ?: emptyList()
                clearSelection()
                notifyDataSetChanged()
            }
        }
    }

    fun clearSelection() {
        selectedItemPos = -1
        notifyDataSetChanged()
    }

    fun sortItemsLowToHigh() {
        filteredItemList = itemList.sortedBy { it.price }
        notifyDataSetChanged()
    }

    fun sortItemsHighToLow() {
        filteredItemList = itemList.sortedByDescending { it.price }
        notifyDataSetChanged()
    }

    fun sortNone() {
        filteredItemList = itemList
        notifyDataSetChanged()
    }
}

const val PESO_SYMBOL = "₱ "
const val TAG_NEW = "NEW"
const val TAG_BEST_SELLER = "BEST SELLER"
const val TAG_PRE_ORDER = "PRE-ORDER"
const val TAG_EMPTY = ""
