package com.example.capstone_track3.gallery.model

import com.example.capstone_track3.R


class ItemRepository {

    private val itemList = mutableListOf<Item>()
    init {
        itemList.apply {
            add(Item("Apple iPhone 14 Pro","apple","iOS",5999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip", R.drawable.apple_iphone_13_red,"599","BEST SELLER","MobilePhone"))
            add(Item("Apple iPhone 13","apple","iOS",3999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_iphone_13_blue,"999","NEW","MobilePhone"))
            add(Item("Apple Watch","apple","iOS",4999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_watch,"799","NEW","Smartwatch"))
            add(Item("Apple iPhone 13","apple","iOS",3499,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_iphone_13_white,"799","PRE-ORDER","MobilePhone"))
            add(Item("Apple Watch","apple","iOS",4999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_watch,"899","NEW","Smartwatch"))
            add(Item("Samsung A14","samsung","android",3799,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.phone_samsung_galaxy_z_flip4_graphite,"599","PRE-ORDER","MobilePhone"))
            add(Item("Apple iPhone 13","apple","iOS",3000,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_iphone_13_blue,"1299","NEW","MobilePhone"))
            add(Item("Apple Watch","apple","iOS",4999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_watch,"999","NEW","Smartwatch"))
            add(Item("Apple iPhone 13 Pro Max","apple","iOS",7999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_iphone_13_blue,"2499","NEW","MobilePhone"))
            add(Item("Samsung A14","samsung","android",8999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.phone_samsung_galaxy_z_flip4_graphite,"799","NEW","MobilePhone"))
            add(Item("Apple iPhone 13 Pro","apple","iOS",6999,"6.1″ Super Retina XDR display | 12MP Main Ultra Wide | A16 Bionic chip",R.drawable.apple_iphone_13_blue,"2999","NEW","MobilePhone"))
        }
    }

    fun getItemList(): List<Item> {
        itemList
        return itemList
    }

}
