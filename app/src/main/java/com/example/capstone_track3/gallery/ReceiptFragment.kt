package com.example.capstone_track3.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.capstone_track3.databinding.FragmentReceiptBinding
import com.example.capstone_track3.gallery.viewModel.PESO_SYMBOL
import com.example.capstone_track3.gallery.viewModel.ReceiptViewModel


class ReceiptFragment : Fragment() {

    lateinit var binding: FragmentReceiptBinding

    private val receiptViewModel: ReceiptViewModel by lazy {
        ViewModelProvider(requireActivity()).get(ReceiptViewModel::class.java)
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentReceiptBinding.inflate(inflater, container, false)


        receiptViewModel.selectedItem.observe(viewLifecycleOwner, Observer { item ->
            binding.tvName.text = item.name
            binding.tvDescription.text = item.description
            binding.tvPrice.text = PESO_SYMBOL+item.price+"/mo."
            binding.tvPlan.text = "GPlan with device " + item.plan
            item.image?.let { binding.ivPhoneImage.setImageResource(it) }
        })

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        return binding.root
    }
}